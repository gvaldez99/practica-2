/*
 * Copyright 2021 gvald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.practica2;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.Scanner;

/*
Clase encargada de leer, manejar y calcular los proximos estados del juego de la vida
*/
public class Tablero{
    private static int DIMENSION = 30;
    private int[][] estadoActual; 
    private int[][] estadoSiguiente; 

    /*
        Metodo encargado de revisar la cantidad de vecinos vivos de una celda
        Complejidad: O(1) ya que siempre va a revisar 8 vecinos ni mas ni menos, la cantidad de operaciones no varia
    */
    private int getVecinosVivos(int x,int y){
        int suma = 0;
        for(int i = -1; i<2; i++){
            for(int j = -1; j<2; j++){
                int col = (x + i + DIMENSION) % DIMENSION; //este calculo hace que los extremos esten conectados y no haya "out of bounds" 
                int fila = (y + j + DIMENSION) % DIMENSION; //por ejemplo x=-1, col se convertiria en el numero DIMENSION-1 llevando al otro extremo en eje X
                suma+=estadoActual[col][fila];
            }
        }
        suma-= estadoActual[x][y];
        return suma;
    }

    /*
        Metodo encargado de calcular el estado siguiente del tablero
        Complejidad: O(n^2) ya que haces dos bucles iterando n veces uno dentro del otro, por lo tanto es multiplicativo
    */
    private int[][] CalcularEstadoSiguiente(){
        int[][] newEstado = new int[DIMENSION][DIMENSION];
        for(int x = 0; x<DIMENSION; x++){
            for(int y = 0; y<DIMENSION; y++){
                int vecinos = getVecinosVivos(x,y);
                if(vecinos >=2 && vecinos<=3 && estadoActual[x][y] == 1){
                    newEstado[x][y] = 1;
                } else if(vecinos ==3 && estadoActual[x][y] == 0) {
                    newEstado[x][y] = 1;
                } else {
                    newEstado[x][y] = 0;
                }
            }
        }
        return newEstado;
    }


    /*
        Metodo encargado de leer el estado del tablero escrito en un fichero de texto y guardar los valores respectivos en el array
        Complejidad: O(n^2) ya que haces dos bucles iterando n veces uno dentro del otro 
    */
    public void leerEstadoActual(){
        File file = new File("matriz.txt");
        estadoActual = new int[DIMENSION][DIMENSION];
        try{
            Scanner sc = new Scanner(file);
            int x = 0;
            while(sc.hasNextLine()){           
                String line = sc.nextLine();
                for(int i=0; i<DIMENSION; i++){
                    estadoActual[x][i] = line.charAt(i) - '0';
                }
                x++;
            }
            
            estadoSiguiente = CalcularEstadoSiguiente();
            
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
    }

    /*
        Metodo encargado de generar un tablero completamente aleatorio, tambien llama al metodo CalcularEstadoSiguiente para ir guardandolo en la variable
        Complejidad: O(n^2) son dos bucles iterando n veces uno dentro del otro
    */
    public void generarEstadoActualPorMontecarlo(){
        for(int x = 0;x<DIMENSION;x++){
            for(int y = 0;y<DIMENSION;y++){
                estadoActual[x][y] = Math.random() >= 0.5 ? 1 : 0;
            }
        }
        estadoSiguiente = CalcularEstadoSiguiente();
    }

    /*
        Metodo encargado de transitar el estado del tablero al siguiente, solo reemplazando los valores de las variables y usando el metodo CalcularEstadoSiguiente
        Complejidad: O(n^2) ya que llama al metodo CalcularEstadoSiguiente con esta complejidad.
    */
    public void transitarAlEstadoSiguiente(){
        estadoActual = estadoSiguiente;
        estadoSiguiente = CalcularEstadoSiguiente();
    }

    /*
        Metodo encargado de convertir el array a texto fila por fila, los 1's a x's y los 0's a un espacio en blanco
        Complejidad: O(n^2) ya que son dos bucles iterando n veces uno dentro del otro
    */
    @Override
    public String toString(){
        String str = "";
        for(int x = 0;x<DIMENSION;x++){
            for(int y = 0;y<DIMENSION;y++){
                str = str + (estadoActual[x][y] == 1 ? "x" : " ");
            }
            str = str + "\n";
        }
        str = str+"------------------------"; //para delimitar iteraciones
         return str;
    }
}
