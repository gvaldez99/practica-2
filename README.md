# README #

## Descripción ##
Proyecto creado con Apache Maven que simula el juego de la vida de Conway, puede leer el tablero inicial de una matriz(fichero matriz.txt) o generar uno al azar.
Analisis de complejidad temporal y descripcion de los metodos encima de cada uno en el codigo.

## Como Compilar y Ejecutar ##

Para ejecutar el proyecto:
```
mvn compile
mvn exec:java -Dexec.mainClass=com.practica2.Principal
```

Para crear un javadoc:
```
mvn javadoc:javadoc
```



Gonzalo Valdez